﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace csharp_test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void menuFileClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        [DllImport("mylib.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Addiction(int a, int b);

        private void btRun_Click(object sender, EventArgs e)
        {
            int a = Convert.ToInt32(tbA.Text);
            int b = Convert.ToInt32(tbB.Text);

            // Использование С++
            int c = Addiction(a, b);

            tbResult.Text = c.ToString();
        }
    }
}
